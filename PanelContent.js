import React, { Component } from 'react';
import { Auth } from './../../../auth.core';
import { APP_CONFIG } from './../../../boot.config';
import { Link, browserHistory } from 'react-router';

import { Col } from 'react-bootstrap';

/** DASHBOARD **/
import { AppManager } from './../../Dashboard/AppManager/AppManager';
import { Product } from './../../Dashboard/Product/Product';
import { Page } from './../../Dashboard/Page/Page';
import { Category } from './../../Dashboard/Category/Category';
import { Settings } from './../../Dashboard/Settings/Settings';

/** SHOP **/
import { ShopProductGrid } from './../../Shop/ProductGrid/ShopProductGrid';
import { CartBox } from './../../Shop/CartBox/CartBox';
export class PanelContent extends Component{
  constructor(props){
    super(props);

    this.renderDashboard = this.renderDashboard.bind(this);
    this.renderShop = this.renderShop.bind(this);
  }

  renderDashboard(){
    if(this.props.panelType === 'AppManager'){
      if( this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR && this.props.buttonsAction ){
        const renderBreadcrumb = () => {
          if(this.props.action === 'new')
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li><Link to={'/dashboard/apps'}>Apps</Link></li>
                <li className="active">Novo</li>
              </ol>
            );

          if(this.props.action === 'edit')
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li><Link to={'/dashboard/apps'}>Apps</Link></li>
                <li className="active">Editar App</li>
              </ol>
            );

          if(!this.props.action)
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li className="active"><Link to={'/dashboard/apps'}>Todos os Apps</Link></li>
              </ol>
            );
        }
        const renderPage = () => {
          if(this.props.action){
            if(this.props.appID)
              return (<AppManager currentUser={this.props.currentUser} action={this.props.action} appID={this.props.appID} />);
            return (<AppManager currentUser={this.props.currentUser} action={this.props.action} />);
          }else{
            return (<AppManager currentUser={this.props.currentUser} />);
          }
        }
        return (
          <div className={'panel panel-info'}>
            <div className="panel-heading">
              {this.props.buttonsAction}
            </div>
            { renderBreadcrumb() }
            <div className="panel-body">
            { renderPage() }
            </div>
          </div>
        );
      }else{
        return (<Auth modeForm={'vertical'} />);
      }
    }else if(this.props.panelType === 'product'){
      if( this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR && this.props.buttonsAction ){
        const renderBreadcrumb = () => {
          if(this.props.action === 'new')
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li><Link to={'/dashboard/products'}>Produtos</Link></li>
                <li className="active">Novo</li>
              </ol>
            );

          if(this.props.action === 'edit')
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li><Link to={'/dashboard/products'}>Produtos</Link></li>
                <li className="active">Editar Produto</li>
              </ol>
            );

          if(!this.props.action)
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li className="active"><Link to={'/dashboard/products'}>Todos os Produtos</Link></li>
              </ol>
            );
        }
        const renderPage = () => {
          if(this.props.action){
            if(this.props.productID)
              return (<Product currentUser={this.props.currentUser} action={this.props.action} productID={this.props.productID} />);
            return (<Product currentUser={this.props.currentUser} action={this.props.action} />);
          }else{
            return (<Product currentUser={this.props.currentUser} />);
          }
        }
        return (
          <div className={'panel panel-info'}>
            <div className="panel-heading">
              {this.props.buttonsAction}
            </div>
            {renderBreadcrumb()}
            <div className="panel-body">
            { renderPage() }
            </div>
          </div>
        );
      }else{
        return (<Auth modeForm={'vertical'} />);
      }
    }else if(this.props.panelType === 'page'){
      if( this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR && this.props.buttonsAction ){
        const renderBreadcrumb = () => {
          if(this.props.action === 'new')
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li><Link to={'/dashboard/page'}>Conteúdo</Link></li>
                <li className="active">Novo</li>
              </ol>
            );

          if(this.props.action === 'edit')
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li><Link to={'/dashboard/page'}>Conteúdo</Link></li>
                <li className="active">Editar</li>
              </ol>
            );

          if(!this.props.action)
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li className="active">Conteúdo</li>
              </ol>
            );
        }
        const renderPage = () => {
          if(this.props.action){
            if(this.props.pageID)
              return (<Page currentUser={this.props.currentUser} action={this.props.action} pageID={this.props.pageID} />);
            return (<Page currentUser={this.props.currentUser} action={this.props.action} />);
          }else{
            return (<Page currentUser={this.props.currentUser} />);
          }
        }
        return (
          <div className={'panel panel-info'}>
            <div className="panel-heading">
              {this.props.buttonsAction}
            </div>
            {renderBreadcrumb()}
            <div className="panel-body">
            { renderPage() }
            </div>
          </div>
        );
      }else{
        return (<Auth modeForm={'vertical'} />);
      }
    }else if(this.props.panelType === 'settings'){
      if( this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR ){
        const renderBreadcrumb = () => {
          if(!this.props.action)
            return (
              <ol className="breadcrumb">
                <li><Link to={'/dashboard'}>Dashboard</Link></li>
                <li className={'active'}>Configurações</li>
              </ol>
            );
        }
        const renderPage = () => {
          if(this.props.action){
            return (<Settings currentUser={this.props.currentUser} action={this.props.action} packages={this.props.propsExtra.packages} logoURL={this.props.propsExtra.logoURL} />);
          }else{
            return (<Settings currentUser={this.props.currentUser} packages={this.props.propsExtra.packages} logoURL={this.props.propsExtra.logoURL} />);
          }
        }
        return (
          <div className={'panel panel-info'}>
            <div className="panel-heading">
              {this.props.buttonsAction ? this.props.buttonsAction : null}
            </div>
            {renderBreadcrumb()}
            <div className="panel-body">
              { renderPage() }
            </div>
          </div>
        );
      }else{
        return (<Auth modeForm={'vertical'} />);
      }
    }else if(this.props.panelType === 'category'){
      if( this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR && this.props.buttonsAction ){
        const renderPage = () => {
          if(this.props.action){
            if(this.props.categoryID)
              return (<Category currentUser={this.props.currentUser} action={this.props.action} categoryID={this.props.categoryID} />);
            return (<Category currentUser={this.props.currentUser} action={this.props.action} />);
          }else{
            return (<Category currentUser={this.props.currentUser} />);
          }
        }
        return (
          <div className={'panel panel-info'}>
            <ol className="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="#">Library</a></li>
              <li className="active">Data</li>
            </ol>
            <div className="panel-heading">
              {this.props.buttonsAction}
            </div>
            <div className="panel-body">
            { renderPage() }

            </div>
          </div>
        );
      }else{
        return (<Auth modeForm={'vertical'} />);
      }
    }
  };//renderDashboard();

  renderShop(){
    const currentPath = browserHistory.getCurrentLocation().pathname;
    let className;
    if(this.props.panelType === 'ShopProductGrid'){
      if(currentPath === '/'){
        className = 'home';
      }else{
        className = '';
      }
      return (
        <div className={'panel'}>
          { this.props.buttonsAction ?
            <div className="panel-heading">
              {this.props.buttonsAction}
            </div>
          :
            ''
          }
          <div className="panel-body no-paddingTop no-paddingLeft">
            <ShopProductGrid
              className={className}
              products={this.props.propsExtra}
              layoutComponent={{itemsPerLine:4, titleGrid:null}}
              />
          </div>
        </div>
      );
    }else if(this.props.panelType === 'ShopCart'){
      if(currentPath === '/'){
        className = 'home';
      }else{
        className = '';
      }
      const renderBreadcrumb = () => {
        return (
          <ol className="breadcrumb">
            <li><Link to={'/'}>Home</Link></li>
            <li className={'active'}>Meu Carrinho</li>
          </ol>
        );
      }
      return (
        <div className={'panel'}>
          { this.props.buttonsAction ?
            <div className="panel-heading">
              {this.props.buttonsAction}
            </div>
          :
            ''
          }
          {renderBreadcrumb()}
          <div className="panel-body no-padding">
            <CartBox
              shopCart
            />
          </div>
        </div>
      );
    }else if(this.props.panelType === 'ShopLogin'){
      if(currentPath === '/'){
        className = 'home';
      }else{
        className = '';
      }
      const renderBreadcrumb = () => {
        return (
          <ol className="breadcrumb">
            <li><Link to={'/'}>Home</Link></li>
            <li className={'active'}>Login</li>
          </ol>
        );
      }
      return (
        <div className={'panel'}>
          { this.props.buttonsAction ?
            <div className="panel-heading">
              {this.props.buttonsAction}
            </div>
          :
            ''
          }
          {renderBreadcrumb()}
          <div className={'panel-body'}>
          <Col xs={12} md={12} className={'no-padding'}>
            <Col xs={12} md={5} className={'sigleBox no-padding'}>
              <Col xs={12} md={12} className={'cartListHover active shopCart no-padding'}>
                <h2 className={'title'}>Já tenho uma conta</h2>
                <Auth
                  modeForm={'vertical'}
                  currentUser={this.props.currentUser}
                  isVisitor={this.props.isVisitor}
                  shopLogin
                />
              </Col>
            </Col>
            <Col xs={12} md={5} className={'sigleBox no-padding pull-right'}>
              <h2 className={'title'}>Criar uma conta</h2>
              <ul>
              </ul>
            </Col>
          </Col>
          </div>
        </div>
      );
    }
  };//renderShop();

  render(){
    if(this.props.where === 'dashboard'){
      return this.renderDashboard();
    }else if(this.props.where === 'shop'){
      return this.renderShop();
    }else{
      return this.renderDashboard();
    }
  }
}
